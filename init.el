;; -*- lexical-binding: t; -*-

(load "server")
(unless (server-running-p) (server-start))

;; Require this really early so that local-lisp can use it
(require 'cl-lib)

(let ((default-directory "~/.emacs.d/local-lisp/"))
  (normal-top-level-add-to-load-path '("."))
  (normal-top-level-add-subdirs-to-load-path))

(require 'compat-hacks)

(require 'system-detect)
(require 'custom-helpers)

(require 'package)

(require 'ert)
(require 'whitespace)

(require 'hyper7)

(require 'rotate-split)
(require 'lisp-family)
(require 'interactify-count-lines)
(require 'sora-misc "~/.emacs.d/local-lisp/misc.el")

(with-eval-after-load 'paredit
  (require 'paredit-menu))

(with-eval-after-load 'vterm
  (keymap-set vterm-mode-map "C-q" #'vterm-send-next-key))

(require 'ace-window)
(keymap-global-set "C-x o" #'ace-window)
(windmove-default-keybindings)

(require 'firestorm-notes)
(firestorm-notes/open-this-weeks-file)

(require 'eldoc)
(eldoc-add-command
 'paredit-backward-delete
 'paredit-close-round)

(keymap-global-set "C-x C-b" #'ibuffer)

(setf disabled-command-function nil)
(setf common-lisp-hyperspec-root "file:/home/Sora/Programming/lisp/HyperSpec/")

;; Load customize.el last, so that everything it may depend on is already loaded

(load "~/.emacs.d/customize.el")

(defun sora/add-to-exec-path (new-path-dir)
  (cl-pushnew new-path-dir exec-path)
  (unless (string-match-p (regexp-quote new-path-dir)
			  (getenv "PATH"))
    (setenv "PATH"
	    (format "%s:%s"
		    new-path-dir
		    (getenv "PATH")))))

(sora/add-to-exec-path (expand-file-name "~/bin"))
(sora/add-to-exec-path (expand-file-name "~/.local/bin"))

;; Now load packages, because now the selected packages variable has been loaded

; NOTE: Add MELPA in this way, as opposed to in `M-x customize`, so that if the
; default underlying list changes at all, it gets applied automatically (a
; hard-learned lesson with `exec-path`...).
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))

(package-install-selected-packages)

