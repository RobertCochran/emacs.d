;; -*- lexical-binding: t; -*-

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(after-save-hook '(executable-make-buffer-file-executable-if-script-p))
 '(auth-source-save-behavior nil)
 '(blink-cursor-blinks -1)
 '(blink-cursor-mode t)
 '(c-basic-offset 8)
 '(c-default-style
   '((c-mode . "linux") (c++-mode . "linux") (java-mode . "java")
     (awk-mode . "awk") (other . "linux")))
 '(c-report-syntactic-errors t)
 '(c-tab-always-indent t)
 '(column-number-mode t)
 '(custom-enabled-themes '(modus-vivendi))
 '(custom-file "~/.emacs.d/customize.el")
 '(default-frame-alist
   '((width . 80) (vertical-scroll-bars . right) (alpha-background . 75)))
 '(desktop-files-not-to-save "\"\"")
 '(desktop-load-locked-desktop t)
 (if system-detect/portablep
     '(display-battery-mode t))
 '(display-raw-bytes-as-hex t)
 '(display-time-mode t)
 '(eat-eshell-mode t)
 '(emacs-lisp-mode-hook
   '(lisp-family-run-hooks eldoc-mode ert--activate-font-lock-keywords
			   sora/elisp-lexical-scope-for-new-files))
 '(eshell-cmpl-cycle-completions nil)
 '(eshell-mode-hook '((lambda nil (setq pcomplete-cycle-completions nil))))
 '(eshell-prompt-function 'sora/eshell-prompt)
 '(eshell-visual-commands nil)
 '(fido-mode t)
 '(fill-column 80)
 '(font-use-system-font t)
 '(gdb-many-windows t)
 '(geiser-guile-binary "guile2.2")
 '(gmm-tool-bar-style 'retro t)
 '(gnus-auto-expirable-marks '(89 77 88 69))
 '(gnus-secondary-select-methods
   '((nnimap "erado" (nnimap-server-port "imaps")
	     (nnimap-address "outlook.office365.com") (nnimap-stream ssl))))
 '(gnus-select-method
   '(nnimap "cochranmail" (nnimap-address "mail.cochranmail.com")
	    (nnimap-server-port "imaps") (nnimap-stream ssl)))
 '(gnus-thread-sort-functions '(gnus-thread-sort-by-most-recent-date))
 '(hexl-iso "-iso")
 '(ielm-mode-hook '(eldoc-mode enable-paredit-mode))
 '(inferior-lisp-program "sbcl")
 '(inhibit-startup-screen t)
 '(lisp-family-hook '(enable-paredit-mode))
 '(lisp-interaction-mode-hook '(lisp-family-run-hooks sora/enable-lexical-binding))
 '(lua-indent-level 4)
 '(mail-mode-hook '(auto-fill-mode))
 '(mail-user-agent 'gnus-user-agent)
 '(make-backup-files nil)
 '(menu-bar-mode nil)
 '(message-send-mail-function 'smtpmail-send-it)
 '(mm-discouraged-alternatives '("text/html" "text/richtext"))
 '(nnmail-expiry-wait 'immediate)
 '(org-agenda-files '("~/notes/"))
 '(org-fold-catch-invisible-edits 'show)
 '(org-mode-hook
   '(org-indent-mode
     #[nil "\300\301\302\303\304$\207"
	   [org-add-hook change-major-mode-hook org-show-block-all append local]
	   5]
     #[nil "\300\301\302\303\304$\207"
	   [org-add-hook change-major-mode-hook org-babel-show-result-all append
			 local]
	   5]
     org-babel-result-hide-spec org-babel-hide-all-hashes))
 '(org-src-fontify-natively t)
 '(package-selected-packages
   '(ace-window auctex auto-complete bitpack buttercup cider codespaces
		command-log-mode csharp-mode eat editorconfig elysium
		fennel-mode forth-mode geiser gnuplot gnuplot-mode go-mode gptel
		htmlize hy-mode impatient-mode lua-mode macrostep magit
		markdown-mode neato-graph-bar nov package-lint paredit
		paredit-menu php-mode pkg-info python-pytest quelpa-use-package
		queue rainbow-delimiters realgud repl-driven-development
		restclient rmsbolt simple-httpd sly spinner suggest
		swagger-to-org tab-helper terraform-mode typescript-mode vterm
		web-mode window-purpose yaml-mode yasnippet))
 '(pcomplete-cycle-completions nil)
 '(proced-auto-update-flag t)
 '(proced-auto-update-interval 1)
 '(prog-mode-hook '(whitespace-mode))
 '(ps-print-header nil)
 (if system-detect/workp
     '(python-pytest-executable "/home/robertc/bin/pytest-peach.sh"))
 '(python-shell-interpreter "python3")
 (if system-detect/android-native-p
     '(shell-file-name "/data/data/com.termux/files/usr/bin/bash"))
 '(show-paren-delay 0)
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(smtpmail-smtp-server "mail.cochranmail.com")
 '(smtpmail-smtp-service 587)
 '(sql-mysql-options '("--no-beep"))
 '(standard-indent 8)
 '(tab-bar-format
   '(tab-bar-format-menu-bar tab-bar-format-history tab-bar-format-tabs
			     tab-bar-separator tab-bar-format-add-tab))
 '(tab-bar-mode t)
 '(user-mail-address "robert@cochranmail.com")
 (when system-detect/chromebookp
   '(vterm-module-cmake-args "-DUSE_SYSTEM_LIBVTERM=no"))
 '(whitespace-style '(face trailing lines-tail)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 (if system-detect/android-native-p
     '(default ((t ( :weight regular :height 138 :width normal :foundry "OG" :family "Roboto Mono"))))
   '(default ((t (:weight regular :height 110 :width normal :foundry "pyrs" :family "Roboto Mono")))))
 '(cursor ((t (:background "white"))))
 '(italic ((t (:slant italic))))
 '(mode-line-active ((t (:inherit mode-line)))))
