#!/bin/sh

git fetch --depth=1 origin master
git checkout FETCH_HEAD
git branch -f master FETCH_HEAD
git checkout master