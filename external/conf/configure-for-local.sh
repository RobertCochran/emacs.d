#!/bin/sh

./configure --prefix="$HOME"/.local --with-pgtk --with-native-compilation --with-imagemagick --with-tree-sitter --without-pop --build x86_64-linux-gnu 'CFLAGS=-DMAIL_USE_LOCKF -O2 -g -pipe -Wall -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -march=native' LDFLAGS=-Wl,-z,relro
