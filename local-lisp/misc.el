;; -*- lexical-binding: t -*-

;;;; Small dumb things that don't deserve their own file and don't fit elsewhere

(defun sora/shrug ()
  (interactive)
  (insert "¯\\_(ツ)_/¯"))

(defun sora/now-as-8601 (&optional arg)
  "Get current time as an ISO 8601 timestep.

No prefix arg means to show in minibuffer.
Single \\[universal-argument] means to insert into buffer.
Double \\[universal-argument] means to insert into buffer quoted."
  (interactive "P")
  (let ((res (string-trim (shell-command-to-string "date -Iseconds"))))
    (when (called-interactively-p)
      (cl-case (car arg)
        (16 (insert "\"" res "\""))
        (4 (insert res))
        (t (message "%s" res))))
    res))

(defun sora/buffer-file-name ()
  (interactive)
  (message "%s" buffer-file-name))

(provide 'sora-misc)
