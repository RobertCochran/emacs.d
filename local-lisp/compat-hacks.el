;; -*- lexical-binding: t -*-

;;;; Compatibility hacks to keep most of the rest of the config clean of
;;;; backwards compatibility cruft as best as is possible. In general,
;;;; compat-hacks will endeavor to bring forward compatibility to older Emacsen,
;;;; so that the config can reasonably worry-free use new functions and
;;;; facilities, instead of trying to faff about trying to get old code to work
;;;; in new environments. This strategy makes the most sense due to using Emacs
;;;; from git, where I will get deprecations and removals and changes faster
;;;; than most. Because of this, versions 'releases' are relative to when the
;;;; master branch does a version cut and moves on to the next major release
;;;; number.
;;;;
;;;; I intend on going through and removing compat hacks as their use becomes
;;;; less necessary to keep this file under control. At the moment, hacks will
;;;; be removed when <hack-for + 2> releases. That is, a hack for functionality
;;;; added in Emacs 30 will be removed when Savannah's master branch major
;;;; version becomes 32.
;;;;
;;;; Because I'm often *not* using older Emacs versions on a regular basis, most
;;;; of the compability hacks will be untested.

;;; Emacs 29 introduces new key binding functions that are more obviously named,
;;; and also always have the bind syntax of `kbd'.
(unless (locate-library "keymap")
  (defun keymap-global-set (key command)
    "COMPATIBILITY HACK FOR EMACS <29. Docstring from 29:

Give KEY a global binding as COMMAND.
COMMAND is the command definition to use; usually it is
a symbol naming an interactively-callable function.

KEY is a string that satisfies `key-valid-p'.

Note that if KEY has a local binding in the current buffer,
that local binding will continue to shadow any global binding
that you make with this function."
    (global-set-key (kbd key) command)))

(provide 'compat-hacks)
