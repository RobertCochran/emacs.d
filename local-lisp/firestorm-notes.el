;; -*- lexical-binding: t -*-

(defun firestorm-notes/get-sunday ()
 (let ((day-of-week (string-to-number (format-time-string "%w"))))
  (time-subtract (current-time)
		 (seconds-to-time (* day-of-week
				     (* 60 60 24))))))

(defun firestorm-notes/make-week-file-name ()
 (concat "notes-"
	 (format-time-string "%m-%d-%y" (firestorm-notes/get-sunday))
	 ".org"))

(defun firestorm-notes/open-this-weeks-file ()
 (interactive)
 (find-file (concat "~/notes/"
		    (firestorm-notes/make-week-file-name))))

(provide 'firestorm-notes)
