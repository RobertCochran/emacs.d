;; -*- lexical-binding: t -*-

(define-advice count-lines (:around (old-count start end) interactify-count-lines)
  (interactive "r")
  (let ((output (funcall old-count start end)))
    (if (called-interactively-p)
        (message "%d" output)
      output)))

(provide 'interactify-count-lines)
