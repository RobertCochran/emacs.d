;; -*- lexical-binding: t -*-

;; Stuff I've written while trying to problem solve

;; Mostly unused for quite a while... don't be surprised if something has rotted
;; and no longer works the way it should...

(defun sora/pull-out-source ()
  (interactive)
  (while (re-search-forward "--- .*\t(nonexistent)" nil t)
    (print (match-string 0) (get-buffer-create "sources"))))

(defun sora/insert-match-into-new-buffer (existing-buffer regex)
  (interactive "bBuffer to match from: \nMRegex: ")
  (save-excursion
    (goto-char (point-min))
    (let ((new-buffer-name (format "*RE Matches for [ %s ]*" existing-buffer)))
      (with-current-buffer (get-buffer-create new-buffer-name)
        (erase-buffer))
      (save-match-data
        (while (re-search-forward regex nil t)
          (with-current-buffer new-buffer-name
            (insert (match-string 0) "\n"))))
      (pop-to-buffer new-buffer-name))))

(defun sora/forward-delete-whitespace ()
  (interactive)
  (delete-region (point)
                 (progn (skip-chars-forward "[:space:]")
                        (point))))

(defun sora/insertize-sql-result (start end)
  (interactive "r")
  (let* ((region-string (buffer-substring-no-properties start end))
         (kv-lines (with-temp-buffer
                     (insert region-string)
                     (split-string region-string "\n" t))))
    (cl-flet ((split-sql-kv (x) (split-string x ": " nil "[[:space:]]+")))
      (message "%S" (cl-loop
                     for (key value) in (mapcar #'split-sql-kv kv-lines)
                     collect (format "`%s`" key) into sql-keys
                     collect (format "'%s'" value) into sql-values
                     finally (return (concat "("
                                             (string-join sql-keys ", ")
                                             ") VALUES ("
                                             (string-join sql-values ", ")
                                             ")")))))))

;; Exchange download metadata field readers

(defun read-le-bin-num (start end)
  (interactive "r")
  (let ((num 0)
        (number-str (reverse (buffer-substring start end))))
    (cl-loop for pos below (length number-str) do
             (setf num
                   (+ (lsh num 8) (aref number-str pos))))
    (message "%d" num)))

(defun read-le-bin-num-and-insert (start end)
  (interactive "r")
  (let ((num 0)
        (number-str (reverse (buffer-substring start end))))
    (cl-loop for pos below (length number-str) do
             (setf num
                   (+ (lsh num 8) (aref number-str pos))))
    (insert (format "%d" num))))

(defun sora/print-hex ()
  (interactive)
  (backward-kill-sexp)
  (insert (format "#x%02x" (string-to-number (current-kill 0)))))

(defun sora/be-number-from-bytes (bytes)
  (let ((result 0))
    (cl-loop for n in bytes do
             (setf result (+ (lsh result 8) n))
             finally return result)))

(defun sora/print-json-array-as-string (start end)
  (interactive "r")
  (let ((array-str (buffer-substring start end)))
    (message "%S" (apply #'string
                         (mapcar (lambda (x) (encode-char x 'iso-8859-1))
                                 (funcall (if (functionp #'json-parse-string)
                                              #'json-parse-string
                                            #'json-read-from-string)
                                          array-str))))))

(defun sora/print-json-array-as-le-num (hexp start end)
  (interactive "p\nr")
  (let* ((array-str (buffer-substring start end))
         (array-list (funcall (if (functionp #'json-parse-string)
                                  #'json-parse-string
                                #'json-read-from-string)
                              array-str)))
    (message (if (> hexp 1) "#x%02x" "%d")
             (sora/be-number-from-bytes (reverse array-list)))))

(defun sora/print-json-array-as-be-num (hexp start end)
  (interactive "p\nr")
  (let* ((array-str (buffer-substring start end))
         (array-list (funcall (if (functionp #'json-parse-string)
                                  #'json-parse-string
                                #'json-read-from-string)
                              array-str)))
    (message (if (> hexp 1) "#x%02x" "%d")
             (sora/be-number-from-bytes array-list))))

(defun sora/print-json-array-as-filetime (start end)
  (interactive "r")
  (let* ((array-str (buffer-substring start end))
         (array-list (funcall (if (functionp #'json-parse-string)
                                  #'json-parse-string
                                #'json-read-from-string)
                              array-str)))
    (message "%s"
             (format-time-string "%c" (seconds-to-time
                                       ; Normalize FILETIME timestamp to Unix timestamp
                                       (- (* (sora/be-number-from-bytes (reverse array-list))
                                             1e-7)
                                          (* 60 60 24 365.2422 369)))))))

(defun sora/print-json-array-as-filetime-weird (start end)
  (interactive "r")
  (let* ((array-str (buffer-substring start end))
         (array-list (funcall (if (functionp #'json-parse-string)
                                  #'json-parse-string
                                #'json-read-from-string)
                              array-str)))
    (message "%s"
             (format-time-string "%c" (seconds-to-time
                                       ; Normalize weird FILETIME timestamp to Unix timestamp
                                       (- (* (sora/be-number-from-bytes (reverse array-list))
                                             1e-7)
                                          (* 60 60 24 365.2422 1969)))))))

;; Magit browsing easiness

(defun sora/magit-toggle-where-mouse-is (ev)
  (interactive "@e")
  (save-excursion
    (setf (point) (posn-point (event-start ev)))
    (magit-section-toggle (magit-current-section))))

(define-key magit-mode-map (kbd "<mouse-3>") #'sora/magit-toggle-where-mouse-is)
(define-key magit-log-mode-map (kbd "<mouse-1>") #'magit-show-commit)
(define-key magit-revision-mode-map (kbd "<mouse-6>") #'scroll-right)
(define-key magit-revision-mode-map (kbd "<mouse-7>") #'scroll-left)
(define-key magit-revision-mode-map (kbd "<mouse-12>") #'other-window)
(define-key magit-revision-mode-map (kbd "<mouse-8>") #'magit-go-backward)
(define-key magit-revision-mode-map (kbd "<mouse-9>") #'magit-go-forward)

(defun sora/open-bug-in-browser (ev)
  (interactive "@e")
  (let ((browse-url-firefox-new-window-is-tab t))
    (save-excursion
      (save-match-data
        (setf (point) (posn-point (event-start ev)))
        (forward-line 0)
        (let ((match-data
               (re-search-forward "\\(fixes\\|refs\\) #?\\([[:digit:]]+\\)" (line-end-position) t)))
          (if match-data
              (browse-url-firefox (concat "<FORGE URL>/issues/"
                                          (match-string 2))
                                  t)))))))

(when nil
  (define-key magit-log-mode-map (kbd "<mouse-3>") #'sora/open-bug-in-browser))

(when nil
  ;; Info mousing niceness

  ;; back
  (define-key Info-mode-map (kbd "<mouse-8>") #'Info-history-back)
  ;; forward
  (define-key Info-mode-map (kbd "<mouse-9>") #'Info-history-forward)

  (define-key help-mode-map (kbd "<mouse-8>") #'help-go-back)
  (define-key help-mode-map (kbd "<mouse-9>") #'help-go-forward))

(defun sora/list-perms (lists)
  (if (null lists)
      '(nil)
    (cl-destructuring-bind (head . tail) lists
      (cl-loop for item in head append
               (cl-loop for perm in (sora/list-perms tail) collect
                        (cons item perm))))))

(defun sora/gen-monitor-line ()
  (interactive)
  (let ((uuid1 (substring (shell-command-to-string "uuidgen") 0 -1))
        (uuid2 (substring (shell-command-to-string "uuidgen") 0 -1)))
    (insert "INSERT INTO <my table> "
            "(uuid, time, found, elastic_id) "
            "VALUES ('" uuid1 "', NOW(), 'N', '" uuid2 "');")))
