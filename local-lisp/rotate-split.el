;; -*- lexical-binding: t -*-

(defun sora/rotate-split ()
  "Rotate a two window split.

Turns a vertical split into a horizontal split, and vice versa.

Will not rotate when there are not exactly two open windows."
  (interactive)
  (unless (= (length (window-list)) 2)
    (user-error "Window count is not exactly 2. Not rotating."))
  (cl-destructuring-bind
      (vertical-split-p edges root-window child-window) (cl-first (window-tree))
    (let ((split-fun (if vertical-split-p
			 #'split-window-right
		       #'split-window-below))
	  (child-buffer (window-buffer child-window))
	  (child-point (window-point child-window))
          (current-window (selected-window))
	  new-window)
      (delete-other-windows root-window)
      (setf new-window (funcall split-fun)
            (window-buffer new-window) child-buffer
            (window-point new-window) child-point)
      (unless (eq current-window root-window)
        (select-window new-window)))))

(provide 'rotate-split)
