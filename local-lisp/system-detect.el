;; -*- lexical-binding: t -*-
;; Variables used for platform detection in init

(defvar system-detect/android-native-p
  (string= system-type "android")
  "Are we on Android via the native port? Non-nil if so.")

(defvar system-detect/android-termux-p
  (cl-some (lambda (p)
		     (string-match-p (regexp-quote "/data/data/com.termux/files/")
				     p))
		   load-path)
  "Are we on Android via Termux? Non-nil if so.")

(defvar system-detect/androidp
  (or system-detect/android-native-p
      system-detect/android-termux-p)
  "Are we on Android at all? Non-nil if so.")

(defvar system-detect/win-nt-p
  (or (string= system-type "windows-nt")
      (string= system-type "cygwin"))
  "Are we on Windows NT at all? Non-nil if so.")

(defvar system-detect/chromebookp
  (string= (system-name) "penguin")
  "Are we on a Chromebook (via Crostini)? Non-nil if so.")

(defvar system-detect/portablep
  (cl-notany (lambda (x) (string= (system-name) x))
             '("SoraDesktop" "SoraDesktopNT"))
  "Are we using a portable computer of some sort? Non-nil if so.")

(defvar system-detect/workp
  (string= (system-name) "peach-laptop")
  "Are we on a work computer? Non-nil if so.")

(provide 'system-detect)
