;; -*- lexical-binding: t -*-

(defun sora/enable-lexical-binding ()
  (setf lexical-binding t))

(defun sora/buffer-visiting-non-existant-file-p ()
  (let ((buffer-file (buffer-file-name)))
    (and buffer-file
         (not (file-exists-p buffer-file)))))

(defun sora/elisp-lexical-scope-for-new-files ()
  (when (sora/buffer-visiting-non-existant-file-p)
    (save-excursion
      (goto-char 0)
      (insert ";; -*- lexical-binding: t -*-\n\n"))
    (setf lexical-binding t)
    ;; Make it possible to immediately kill the new buffer without having
    ;; to double-check because of the mechanical insert
    (set-buffer-modified-p nil)))

(defun sora/eshell-prompt ()
  "Makes a much shorter, bash-like prompt for TRAMP paths.

Looks something like `user@example mydir $'.

Has not been verified to work on Windows!"
  (let ((wd (abbreviate-file-name (eshell/pwd))))
    (if (not (featurep 'tramp))
        ;; By definition then, we don't have to worry about remote paths
        (concat wd (if (zerop (user-uid)) " # " " $ "))
      (cl-destructuring-bind (regex _method user host file _hop)
          tramp-file-name-structure
        (save-match-data
          (concat (if (not (string-match regex wd))
                      wd
                    (concat (match-string user wd)
                            "@"
                            (match-string host wd)
                            " "
                            (let* ((original-path (match-string file wd))
                                   (split-path (split-string original-path
                                                             "\\/"
                                                             t)))
                              (if (null split-path)
                                  "/"
                                (car (last split-path))))))
                  (if (or (zerop (user-uid))
                          (string= "root" (match-string user wd)))
                      " # "
                    " $ ")))))))

(provide 'custom-helpers)
