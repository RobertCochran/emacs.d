;; -*- lexical-binding: t -*-

;; Things for Hyper7 keyboard

(defvar sora/using-hyper-7-p
  "Are we on a machine using the Hyper7? Non-nil if so."
  (string= (system-name) "SoraDesktop"))

(keymap-global-set "<XF86Cut>" #'kill-region)
(keymap-global-set "<XF86Paste>" #'yank)

(provide 'hyper7)

