;; -*- lexical-binding: t -*-

(defcustom lisp-family-hook
  nil
  "Hook for all Lisp family langauges."
  :type 'hook
  :group 'lisp)

(defcustom lisp-family-mode-hooks
  '(emacs-lisp-mode-hook
    lisp-interaction-mode-hook
    lisp-mode-hook
    scheme-mode-hook
    clojure-mode-hook
    sly-mrepl-mode-hook
    cider-repl-mode-hook)
  "Hook variables for modes that are considered Lisp family."
  :group 'lisp)

(defun lisp-family-run-hooks ()
  "Run the hooks in `lisp-family-hook'."
  (run-hooks 'lisp-family-hook))

(dolist (mode-hook lisp-family-mode-hooks)
  (add-hook mode-hook #'lisp-family-run-hooks))

(provide 'lisp-family)
